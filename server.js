var http = require('http');
var url = require('url');
var fs = require('fs');

var app = http.createServer(function(request, response) { 
	if (request.url === '/favicon.ico') {
    response.writeHead(200, {'Content-Type': 'image/x-icon'} );
    response.end();
    console.log('favicon requested');
    return;
  }
	// handle the routes
  	if (request.method === 'GET') {
		var url_request = url.parse(request.url).pathname;
		var tmp  = url_request.lastIndexOf(".");
    	var extension  = url_request.substring((tmp + 1));
    	console.log(url_request);
    	if (url_request === "/")
    	{
    		url_request = "/index.html";
    	}
    	var filePath = url_request.replace("/", "")

		fs.readFile(filePath, function (err, html) {
			if (extension === 'html') {
				response.writeHeader(200, {"Content-Type": 'text/html'});
			}
          	else if (extension === 'css') {
          		response.writeHeader(200, {"Content-Type": 'text/css'});
          	}
          	else if (extension === 'js') {
          		response.writeHeader(200, {"Content-Type": 'text/javascript'});
          	}
          	else if (extension === 'png') {
          		response.writeHeader(200, {"Content-Type": 'image/png'});
          	}
	        response.write(html);  
	        response.end();  
		});
	}else{
		var mail = require("nodemailer").mail;

		mail({
		    from: "Campidee <jderenty@gmail.com>", // sender address
		    to: request.body.email, // list of receivers
		    subject: "Une nouvelle idée pour vous", // Subject line
		    text: request.body.text, // plaintext body
		    html: request.body.text // html body
		});	
	}
}).listen(Number(process.env.PORT || 5000));


